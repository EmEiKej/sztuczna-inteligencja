﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Engineering.Implementations;

namespace Atomic_orbital_search
{
    public enum E_Test_Functions_Names { RastriginFunction, RosenbrockFunction, SphereFunction, BealeFunction, BukinFunctionN6, HimmelblausFunctionN6 }
    public class TestFunctions
    {
        public TestFunctions(E_Test_Functions_Names Name_of_function, int D, int M, int Max_Number_Of_I, double _P1, double _Pk)
        {
            name_of_function = Name_of_function;
            d = D;
            m = M;
            max_number_of_I = Max_Number_Of_I;
            P1 = _P1;
            Pk = _Pk;
        }
        E_Test_Functions_Names name_of_function;
        int number_of_tests = 1;
        double minXi;
        double maxXi;
        int d;
        int m;
        int max_number_of_I;
        double P1;
        double Pk;
        List<double> bestCost = new List<double>();
        List<List<double>> LE = new List<List<double>>();
        List<double> sigmaX = new List<double>();
        double sigmaE;
        void ChosenFunction()
        {
            if (name_of_function == E_Test_Functions_Names.RastriginFunction)
            {
                minXi = -5.12;
                maxXi = 5.12;
            }
            else if (name_of_function == E_Test_Functions_Names.RosenbrockFunction)
            {
                minXi = -1;
                maxXi = 1;
            }
            else if (name_of_function == E_Test_Functions_Names.SphereFunction)
            {
                minXi = -1;
                maxXi = 1;
            }
            else if (name_of_function == E_Test_Functions_Names.BealeFunction)
            {
                minXi = -4.5;
                maxXi = 4.5;
                d = 2;
            }
            else if (name_of_function == E_Test_Functions_Names.BukinFunctionN6)
            {
                minXi = -15;
                maxXi = -5;
                d = 2;
            }
            else if (name_of_function == E_Test_Functions_Names.HimmelblausFunctionN6)
            {
                minXi = -5;
                maxXi = 5;
                d = 2;
            }
        }
        void SortData()
        {
            var indexes = Enumerable.Range(0, bestCost.Count).OrderBy(i => bestCost[i]).ToList();
            bestCost = indexes.Select(i => bestCost[i]).ToList();
            LE = indexes.Select(i => LE[i]).ToList();
        }
        void Determine_sigmaX()
        {
            List<double> mi = new List<double>();
            List<double> sum = new List<double>();
            for (int j = 0; j < LE[0].Count; j++)
            {
                mi.Add(0);
                for (int i = 0; i < LE.Count; i++)
                {
                    mi[j] += LE[i][j];
                }
                mi[j] /= LE.Count;
            }
            for (int j = 0; j < LE[0].Count; j++)
            {
                sum.Add(0);
                for (int i = 0; i < LE.Count; i++)
                {
                    sum[j] += Math.Pow((LE[i][j] - mi[j]), 2);
                }
            }
            for (int j = 0; j < LE[0].Count; j++)
            {
                sigmaX.Add(Math.Sqrt(sum[j] / LE.Count));
            }
        }
        void Determine_sigmaE()
        {
            double mi = 0;
            double sum = 0;
            mi = bestCost.Average();
            for (int i = 0; i < bestCost.Count; i++)
            {
                sum += Math.Pow((bestCost[i] - mi), 2);
            }
            sigmaE = Math.Sqrt(sum / bestCost.Count);
        }
        public (E_Test_Functions_Names, int, int, int, List<double>, List<double>, double, double, double, double) Main()
        {
            ChosenFunction();
            while (number_of_tests-- != 0)
            {
                AOS aos = new AOS(name_of_function, minXi, maxXi, d, m, max_number_of_I, P1, Pk);
                var result = aos.AOS_Main_Function();
                bestCost.Add(result.Item1);
                LE.Add(new List<double>(result.Item2));
            }
            SortData();
            Determine_sigmaX();
            Determine_sigmaE();
            return (name_of_function, d, m, max_number_of_I, LE[0], sigmaX, bestCost[0], sigmaE, P1, Pk);
        }
        void Show_LE()
        {
            Console.WriteLine("-----------------------------------------------------------------LE");
            foreach (var layer in LE)
            {
                string result = string.Join("\t", layer);
                Console.WriteLine(result);
            }
        }
        void Show_bestCost()
        {
            Console.WriteLine("-----------------------------------------------------------------bestCost");
            Console.WriteLine(string.Join("\n", bestCost));
        }
        void Show_sigmaX()
        {
            Console.WriteLine("-----------------------------------------------------------------sigmaX");
            Console.WriteLine(string.Join("\t", sigmaX));
        }
        void Show_sigmaE()
        {
            Console.WriteLine("-----------------------------------------------------------------sigmaE");
            Console.WriteLine(sigmaE);
        }
        void ShowTestResults()
        {
            Console.WriteLine("Funkcja Testowa : " + name_of_function);
            Console.WriteLine("Liczba szukanych parametrów : " + d);
            Console.WriteLine("Liczba iteracji : " + max_number_of_I);
            Console.WriteLine("Rozmiar populacji : " + m);
            Console.WriteLine("Znalezione minimum : " + string.Join("\t", LE[0]));
            Console.WriteLine("sigmaX : " + string.Join("\t", sigmaX));
            Console.WriteLine("Wartość funkcji celu : " + bestCost[0]);
            Console.WriteLine("sigmaE : " + sigmaE);
        }
    }
}