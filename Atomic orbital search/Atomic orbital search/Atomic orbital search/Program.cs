﻿using Atomic_orbital_search;
using System.Diagnostics;

//Stopwatch stopwatch = new Stopwatch();
//stopwatch.Start();
//List<E_Test_Functions_Names> funkcjaTestowa = new List<E_Test_Functions_Names> { E_Test_Functions_Names.RastriginFunction, E_Test_Functions_Names.RosenbrockFunction, E_Test_Functions_Names.SphereFunction };
//List<int> liczbaSzukanychParametrów = new List<int> { 2, 10, 50 };
//List<int> liczbaIteracji = new List<int> { 5, 40, 80 };
//List<int> rozmiarPopulacji = new List<int> { 10, 50, 100 };
//foreach (E_Test_Functions_Names nazwaFunkcjiTestowej in funkcjaTestowa)
//{
//    foreach (int n in liczbaSzukanychParametrów)
//    {
//        foreach (int N in rozmiarPopulacji)
//        {
//            foreach (int I in liczbaIteracji)
//            {
//                TestFunctions testFunction = new TestFunctions(nazwaFunkcjiTestowej, 2, 10, 100000, 0.1, 0.01);
//                var result = testFunction.Main();
//                Excel excel = new Excel(result.Item1, result.Item2, result.Item3, result.Item4, result.Item5, result.Item6, result.Item7, result.Item8, result.Item9, result.Item10);
//                excel.WriteExcel();
//                Console.WriteLine("\t Funkcja testowa :" + nazwaFunkcjiTestowej + ",\t n : " + n + ",\t  N : " + N + ",\t  I : " + I + " \t Wykonano pomyślnie!");
//            }
//        }
//    }
//}

TestFunctions testFunction = new TestFunctions(E_Test_Functions_Names.BukinFunctionN6, 2, 30, 100, 0.1, 0.01);
var result = testFunction.Main();
Excel excel = new Excel(@"E:\\Atomic orbital search\\Wyniki\\WynikiTestów.xlsx",result.Item1, result.Item2, result.Item3, result.Item4, result.Item5, result.Item6, result.Item7, result.Item8, result.Item9, result.Item10);
excel.WriteExcel();
//stopwatch.Stop();
//Console.WriteLine("Czas wykonywania: " + stopwatch.Elapsed);


