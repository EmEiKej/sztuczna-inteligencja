﻿using Microsoft.Office.Interop.Excel;
using static System.Net.Mime.MediaTypeNames;
using Syncfusion.XlsIO;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;

namespace Atomic_orbital_search
{
    public class Excel
    {
        string filePath;
        E_Test_Functions_Names funkcjaTestowa;
        int liczbaSzukanychParametrów;
        int liczbaIteracji;
        int rozmiarPopulacji;
        List<double> znalezioneMinimum;
        List<double> odchylenieStandardowePoszukiwanychParametrów;
        double wartośćFunkcjiCelu;
        double odchylenieStandardoweFunkcjiCelu;
        double P1;
        double Pk;
        public Excel(string FilePath, E_Test_Functions_Names FunkcjaTestowa, int LiczbaSzukanychParametrów, int LiczbaIteracji, int RozmiarPopulacji, List<double> ZnalezioneMinimum, List<double> OdchylenieStandardowePoszukiwanychParametrów, double WartośćFunkcjiCelu, double OdchylenieStandardoweFunkcjiCelu, double _P1, double _Pk)
        {
            filePath = FilePath;
            funkcjaTestowa = FunkcjaTestowa;
            liczbaSzukanychParametrów = LiczbaSzukanychParametrów;
            liczbaIteracji = LiczbaIteracji;
            rozmiarPopulacji = RozmiarPopulacji;
            znalezioneMinimum = ZnalezioneMinimum;
            odchylenieStandardowePoszukiwanychParametrów = OdchylenieStandardowePoszukiwanychParametrów;
            wartośćFunkcjiCelu = WartośćFunkcjiCelu;
            odchylenieStandardoweFunkcjiCelu = OdchylenieStandardoweFunkcjiCelu;
            P1 = _P1;
            Pk = _Pk;
        }

        public void WriteExcel()
        {
            int i = 1;
            bool loopStatement = true;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            if (!File.Exists(filePath))
            {
                using (var package = new ExcelPackage())
                {

                    var worksheet = package.Workbook.Worksheets.Add("Arkusz1");
                    worksheet.Cells["A1"].Value = "Algorytm";
                    worksheet.Cells["B1"].Value = "Funkcja testowa";
                    worksheet.Cells["C1"].Value = "Liczba szukanych parametrów";
                    worksheet.Cells["D1"].Value = "P1";
                    worksheet.Cells["E1"].Value = "-";
                    worksheet.Cells["F1"].Value = "Pk";
                    worksheet.Cells["G1"].Value = "Liczba iteracji";
                    worksheet.Cells["H1"].Value = "Rozmiar populacji";
                    worksheet.Cells["I1"].Value = "Znalezione minimum";
                    worksheet.Cells["J1"].Value = "Odchylenie standardowe poszukiwanych parametrów";
                    worksheet.Cells["K1"].Value = "Wartość funkcji celu";
                    worksheet.Cells["L1"].Value = "Odchylenie standardowe wartości funkcji celu";
                    package.SaveAs(new FileInfo(filePath));
                }
            }
            using (var package = new ExcelPackage(new FileInfo(filePath)))
            {
                var worksheet = package.Workbook.Worksheets["Arkusz1"];
                while (loopStatement)
                {
                    i++;
                    if (worksheet.Cells[i, 1].Value == null)
                    {
                        loopStatement = false;
                    }
                }
                worksheet.Cells["A" + i].Value = "AOS";
                worksheet.Cells["B" + i].Value = funkcjaTestowa;
                worksheet.Cells["C" + i].Value = liczbaSzukanychParametrów;
                worksheet.Cells["D" + i].Value = P1;
                worksheet.Cells["E" + i].Value = "-";
                worksheet.Cells["F" + i].Value = Pk;
                worksheet.Cells["G" + i].Value = liczbaIteracji;
                worksheet.Cells["H" + i].Value = rozmiarPopulacji;
                worksheet.Cells["I" + i].Value = "( " + string.Join(" | ", znalezioneMinimum) + " )";
                worksheet.Cells["J" + i].Value = "( " + string.Join(" | ", odchylenieStandardowePoszukiwanychParametrów) + " )";
                worksheet.Cells["K" + i].Value = wartośćFunkcjiCelu;
                worksheet.Cells["L" + i].Value = odchylenieStandardoweFunkcjiCelu;
                package.Save();

            }
        }
        public void WriteExcel2()
        {
            int i = 1;
            bool loopStatement = true;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            if (!File.Exists(filePath))
            {
                using (var package = new ExcelPackage())
                {

                    var worksheet = package.Workbook.Worksheets.Add("Arkusz1");
                    worksheet.Cells["A1"].Value = "Liczba iteracji";
                    worksheet.Cells["A2"].Value = "Wartość funkcji celu";
                    package.SaveAs(new FileInfo(filePath));
                }
            }
            using (var package = new ExcelPackage(new FileInfo(filePath)))
            {
                var worksheet = package.Workbook.Worksheets["Arkusz1"];
                while (loopStatement)
                {
                    i++;
                    if (worksheet.Cells[1, i].Value == null)
                    {
                        loopStatement = false;
                    }
                }
                worksheet.Cells[1, i].Value = liczbaIteracji;
                worksheet.Cells[2, i].Value = wartośćFunkcjiCelu;
                package.Save();

            }
        }
    }
}
