﻿using MathNet.Numerics.Distributions;
namespace Atomic_orbital_search
{
    public class AOS
    {
        public AOS(E_Test_Functions_Names ChosenFunction, double MinXi, double MaxXI, int D, int M, int Max_Number_Of_I, double P1, double Pk)
        {
            chosenFunction = ChosenFunction;
            minXi = MinXi;
            maxXi = MaxXI;
            d = D;
            m = M;
            max_number_of_I = Max_Number_Of_I;
            PR = P1;
            PR_Min = Pk;
        }
        E_Test_Functions_Names chosenFunction;
        Random random = new Random();
        double minXi; // Minimum value of candidate's position
        double maxXi; // Maximum value of candidate's position
        int d;  // Position of candidates
        int m; // Number of solution candidates
        List<List<double>> X = new List<List<double>>(); // Solution candidates
        List<double> E = new List<double>(); // Energy level of solution candidates
        List<double> BS = new List<double>(); // Binding state
        double BE; // Binding energy
        List<double> LE = new List<double>(); // Candidate with lowest energy level
        int I = 0; // Iteration counter
        int max_number_of_I; // Number of iterations
        int max_number_of_imaginary_leyers = 5; // Maximum number of imaginary layers
        int n; // Number of imaginary layers
        List<double> PDF = new List<double>(); // PDF of imaginary layers around the atom
        int mu = 1; // Mean  position of candidate
        double sigma = 0.7; // Standard deviation of position candidate
        List<Double> imaginary_Layers_Formula = new List<Double>(); // Formula of distributing solution candidates on imaginary layers
        List<List<List<double>>> Xk = new List<List<List<double>>>(); // Solution candidates on kth imaginary layer
        List<List<double>> Ek = new List<List<double>>();   // Energy level of solution candidates on kth imaginary layer
        List<List<double>> BSk = new List<List<double>>(); // Binding state of kth Xk
        List<double> BEk = new List<double>(); // Binding energy of the kth layer
        List<List<double>> LEk = new List<List<double>>(); // Candidate with lowest energy level on kth layer
        double fi; // Random number (0,1)
        double alfa; // Random number (0,1)
        double beta; // Random number (0,1)
        double gamma; // Random number (0,1)
        double PR; // Internal variable
        double PR_Min; // Minimum value of PR
        double PR_Increasement; // PR increasment per loop
        List<double> r = new List<double>(); // List of random numbers (0,1)
        List<List<List<double>>> Xk1 = new List<List<List<double>>>(); // Upcoming positions for the ith solution candidate of the kth layer
        List<double> ChosenFunction(List<List<double>> x)
        {
            List<double> e = new List<double>();
            if (chosenFunction == E_Test_Functions_Names.RastriginFunction)
            {
                for (int i = 0; i < x.Count; i++)
                {
                    e.Add(10 * x[i].Count);
                    for (int j = 0; j < x[i].Count; j++)
                    {
                        e[i] += Math.Pow(x[i][j], 2) - 10 * Math.Cos(2 * Math.PI * x[i][j]);
                    }
                }
            }
            else if (chosenFunction == E_Test_Functions_Names.RosenbrockFunction)
            {
                for (int i = 0; i < x.Count; i++)
                {
                    e.Add(0);
                    for (int j = 0; j < x[i].Count - 1; j++)
                    {
                        e[i] += 100 * Math.Pow((x[i][j + 1] - Math.Pow(x[i][j], 2)), 2) + Math.Pow((1 - x[i][j]), 2);
                    }
                }
            }
            else if (chosenFunction == E_Test_Functions_Names.SphereFunction)
            {
                for (int i = 0; i < x.Count; i++)
                {
                    e.Add(0);
                    for (int j = 0; j < x[i].Count; j++)
                    {
                        e[i] += Math.Pow(x[i][j], 2);
                    }
                }
            }
            else if (chosenFunction == E_Test_Functions_Names.BealeFunction)
            {
                for (int i = 0; i < x.Count; i++)
                {
                    e.Add(0);
                    e[i] = Math.Pow(1.5 - x[i][0] + x[i][0] * x[i][1], 2) + Math.Pow(2.25 - x[i][0] + x[i][0] * Math.Pow(x[i][1], 2), 2) + Math.Pow(2.625 - x[i][0] + x[i][0] * Math.Pow(x[i][1], 3), 2);
                }
            }
            else if (chosenFunction == E_Test_Functions_Names.BukinFunctionN6)
            {
                for (int i = 0; i < x.Count; i++)
                {
                    e.Add(0);
                    e[i] = 100 * Math.Sqrt(Math.Abs(x[i][1] - 0.01 * Math.Pow(x[i][0], 2))) + 0.01 * Math.Abs(x[i][0] + 10);
                }
            }
            else if (chosenFunction == E_Test_Functions_Names.HimmelblausFunctionN6)
            {
                for (int i = 0; i < x.Count; i++)
                {
                    e.Add(0);
                    e[i] = Math.Pow((Math.Pow(x[i][0], 2) + x[i][1] - 11), 2) + Math.Pow((x[i][0] + Math.Pow(x[i][1], 2) - 7), 2);
                }
            }
            return e;
        }
        void Determine_BS() // Determine binding statea (BS) of atom
        {
            //BS = Enumerable.Range(0, d).Select(j => X.Average(row => row[j])).ToList();
            for (int j = 0; j < d; j++)
            {
                BS.Add(0);
                for (int i = 0; i < m; i++)
                {
                    BS[j] += X[i][j];
                }
                BS[j] /= m;

            }
        }
        void Create_Imaginary_Layers() // Create imaginary layers
        {
            bool stayInLoop = true;
            double cumSum = 0;
            PDF = Enumerable.Range(0, n).Select(i => (double)i).ToList();
            Normal normalDistribution = new Normal(mu, sigma);
            PDF = PDF.Select(x => normalDistribution.Density(x)).ToList();
            imaginary_Layers_Formula = PDF.Select(value => value / PDF.Sum()).ToList();
            imaginary_Layers_Formula = imaginary_Layers_Formula.Select(value => value * m).ToList();
            imaginary_Layers_Formula = imaginary_Layers_Formula.Select(value => Math.Round(value)).ToList();
            imaginary_Layers_Formula = imaginary_Layers_Formula.Select(value => cumSum += value).ToList();
            imaginary_Layers_Formula.Insert(0, 0);
            for (int i = imaginary_Layers_Formula.Count - 1; i > 1; i--)
            {
                if (imaginary_Layers_Formula[i] != m)
                {
                    if (imaginary_Layers_Formula[i] == imaginary_Layers_Formula[i - 1])
                    {
                        imaginary_Layers_Formula[i]--;
                        imaginary_Layers_Formula[i - 1]++;
                    }
                }
            }
            for (int i = 0; i < imaginary_Layers_Formula.Count; i++)
            {
                if (imaginary_Layers_Formula[i] > m)
                {
                    imaginary_Layers_Formula[i] = m;
                }
            }
            imaginary_Layers_Formula[imaginary_Layers_Formula.Count - 1] = m;
        }
        void Sort_Solution_Candidates() // Sort solution candidates in as ascending or descending order
        {
            var indexes = Enumerable.Range(0, E.Count).OrderBy(i => E[i]).ToList();
            E = indexes.Select(i => E[i]).ToList();
            X = indexes.Select(i => X[i]).ToList();
        }
        void Distribute_Xk() // Distribute solution candidates in the imaginary layers by PDF
        {
            int x;
            for (int k = 0; k < n; k++)
            {
                Xk.Add(new List<List<double>>());
                Ek.Add(new List<double>());
                if (imaginary_Layers_Formula[k + 1] - imaginary_Layers_Formula[k] != 0)
                {
                    x = 0;
                    for (int i = (int)imaginary_Layers_Formula[k]; i < imaginary_Layers_Formula[k + 1]; i++)
                    {
                        Ek[k].Add(0);
                        Xk[k].Add(new List<double>());
                        for (int j = 0; j < d; j++)
                        {
                            Xk[k][x].Add(0);
                            Xk[k][x][j] = X[i][j];
                        }
                        Ek[k][x] = E[i];
                        x++;
                    }
                }
            }
        }
        void Determine_BSk_and_BEk(int k) // Determine binding state (BSk) and binding energy (BEk) of the kth layer
        {
            if (imaginary_Layers_Formula[k + 1] - imaginary_Layers_Formula[k] != 0)
            {
                //BSk.Add(new List<double>());
                //BSk[k] = Enumerable.Range(0, d).Select(j => Xk[k].Average(row => row[j])).ToList(); // Determine binding state (BS) of the kth layer
                BEk.Add(0);
                BSk.Add(new List<double>());
                for (int j = 0; j < d; j++)
                {
                    BSk[k].Add(0);
                    for (int i = 0; i < Xk[k].Count; i++)
                    {
                        BSk[k][j] += Xk[k][i][j];
                    }
                    BSk[k][j] /= Xk[k].Count;

                }
                BEk[k] = Ek[k].Average(); // Determine binding energy of the kth layer
            }
        }
        void Determine_LEk(int k) // Determine solution candidates whit lowest energy level in the kth layer(LEk)
        {
            if (imaginary_Layers_Formula[k + 1] - imaginary_Layers_Formula[k] != 0)
            {
                LEk.Add(new List<double>());
                LEk[k] = Xk[k][Ek[k].Select((value, index) => new { Value = value, Index = index }).OrderBy(item => item.Value).First().Index];
            }
        }
        void Generate_fi_alfa_beta_gamma() // Generate fi, alfa, beta, gamma
        {
            fi = random.NextDouble();
            alfa = random.NextDouble();
            beta = random.NextDouble();
            gamma = random.NextDouble();
        }
        void Determine_Xk1_with_r(int k, int i) // Determine upcoming positions for the ith solution candidate of the kth layer
        {
            for (int j = 0; j < d; j++)
            {
                Xk1[k][i].Add(0);
                Xk1[k][i][j] = Xk[k][i][j] + r[i];
            }
        }
        void Determine_Xk1_for_Ek_value_higher_equal_BEk(int k, int i) // Determine upcoming positions for the ith solution candidate of the kth layer for Ek>=BEk
        {
            for (int j = 0; j < d; j++)
            {
                Xk1[k][i].Add(0);
                Xk1[k][i][j] = Xk[k][i][j] + (alfa * (beta * LE[j] - gamma * BS[j]) / (k + 1));
            }
        }
        void Determine_Xk1_for_Ek_value_lower_than_BEk(int k, int i) // Determine upcoming positions for the ith solution candidate of the kth layer for Ek<BEk
        {
            for (int j = 0; j < d; j++)
            {
                Xk1[k][i].Add(0);
                Xk1[k][i][j] = Xk[k][i][j] + (alfa * (beta * LE[j] - gamma * BSk[k][j]));
            }
        }
        void Update_Data() // Update X, E, BS, BE, LE
        {
            PR -= PR_Increasement;
            int x = m;
            for (int k = 0; k < Xk1.Count; k++)
            {
                for (int i = 0; i < Xk1[k].Count; i++)
                {
                    X.Add(new List<double>());
                    for (int j = 0; j < d; j++)
                    {
                        X[x].Add(Xk1[k][i][j]);
                    }
                    x++;
                }
            }
            x = 0;
            E = ChosenFunction(X);
            Sort_Solution_Candidates();
            X.RemoveRange(m, m);
            E.RemoveRange(m, m);
            //BS = Enumerable.Range(0, d).Select(j => X.Average(row => row[j])).ToList();
            BS.Clear();
            Determine_BS();
            BE = E.Average();
            LE = X[E.Select((value, index) => new { Value = value, Index = index }).OrderBy(item => item.Value).First().Index];
        }
        void Restart_Data()
        {
            PDF.Clear();
            imaginary_Layers_Formula.Clear();
            Xk.Clear();
            Ek.Clear();
            BSk.Clear();
            BEk.Clear();
            LEk.Clear();
            Xk1.Clear();

        }
        public (double, List<double>) AOS_Main_Function()
        {
            PR_Increasement = (PR - PR_Min) / max_number_of_I;
            X = Enumerable.Range(0, m).Select(_ => Enumerable.Range(0, d).Select(_ => random.NextDouble() * (maxXi - minXi) + minXi).ToList()).ToList(); // Determine initial positions of solution candidates (Xi) in the search space with m candidates
            E = ChosenFunction(X);  // Evaluate fittness values (Ei) for initial solution candidates
            Determine_BS(); // Determine binding statea (BS) of atom
            BE = E.Average(); // Determine binding energy (BE) of atom
            LE = X[E.Select((value, index) => new { Value = value, Index = index }).OrderBy(item => item.Value).First().Index]; // Determine candidate with lowest energy level in atom (LE)
            r = Enumerable.Range(0, m).Select(_ => random.NextDouble()).ToList(); // Determine r
            while (I++ < max_number_of_I)
            {
                n = random.Next(max_number_of_imaginary_leyers) + 1; //Generate n as number of imaginary layers
                Create_Imaginary_Layers(); // Create imaginary layers
                Sort_Solution_Candidates(); // Sort solution candidates in ascending or descending order
                Distribute_Xk(); // Distribute solution candidates in the imaginary layers by PDF
                for (int k = 0; k < Xk.Count; k++)
                {
                    Xk1.Add(new List<List<double>>());
                    Determine_BSk_and_BEk(k); // Determine binding state (BSk) and binding energy (BEk) of the kth layer
                    Determine_LEk(k); // Determine solution candidates whit lowest energy level in the kth layer(LEk)
                    for (int i = 0; i < Xk[k].Count; i++)
                    {
                        Xk1[k].Add(new List<double>());
                        Generate_fi_alfa_beta_gamma(); // Generate fi, alfa, beta, gamma
                        if (fi >= PR)
                        {
                            if (Ek[k][i] >= BEk[k])
                            {
                                Determine_Xk1_for_Ek_value_higher_equal_BEk(k, i); // Determine upcoming positions for the ith solution candidate of the kth layer for Ek>=BEk
                            }
                            else
                            {
                                Determine_Xk1_for_Ek_value_lower_than_BEk(k, i); // Determine upcoming positions for the ith solution candidate of the kth layer for Ek<BEk
                            }
                        }
                        else
                        {
                            Determine_Xk1_with_r(k, i); // Determine upcoming positions for the ith solution candidate of the kth layer
                        }
                    }
                }
                Update_Data(); // Update X, E, BS, BE, LE
                Restart_Data(); // Restart data for next loop
                //if (I % 10 == 0) 
                //{
                //    Excel excel = new Excel(@"E:\\Atomic orbital search\\Wyniki\\WynikiTestów"+chosenFunction+".xlsx", chosenFunction, d, I, m, LE, BS, E[0], BE, PR, PR_Min) ;
                //    excel.WriteExcel2();
                //}
            }
            return (E[0], LE);

        }
        void Show_X()
        {
            Console.WriteLine("-----------------------------------------------------------------X");
            foreach (var row in X)
            {
                string rowString = string.Join("\t", row.Select(value => value.ToString()));
                Console.WriteLine(rowString);
            }
        }
        void Show_E()
        {
            Console.WriteLine("-----------------------------------------------------------------E");
            string result = string.Join("\n", E);
            Console.WriteLine(result);
        }
        void Show_BS_and_BE()
        {
            Console.WriteLine("-----------------------------------------------------------------BS");
            string result = string.Join("\t", BS);
            Console.WriteLine(result);
            Console.WriteLine("-----------------------------------------------------------------BE");
            Console.WriteLine(BE);
        }
        void Show_LE()
        {
            Console.WriteLine("-----------------------------------------------------------------LE");
            string result = string.Join("\t", LE);
            Console.WriteLine(result);
        }
        void Show_n()
        {
            Console.WriteLine("-----------------------------------------------------------------n");
            Console.WriteLine(n);
        }
        void Show_Imaginary_Layers()
        {
            Console.WriteLine("-----------------------------------------------------------------PDF");
            string result = string.Join("\t", PDF);
            Console.WriteLine(result);
            Console.WriteLine("-----------------------------------------------------------------imaginary_Layers_Formula");
            string result1 = string.Join("\t", imaginary_Layers_Formula);
            Console.WriteLine(result1);
        }
        void Show_Xk()
        {
            Console.WriteLine("-----------------------------------------------------------------Xk");
            foreach (var X in Xk)
            {
                Console.WriteLine("--------Next layer");
                foreach (var row in X)
                {
                    foreach (var value in row)
                    {
                        Console.Write("\t" + value);
                    }
                    Console.WriteLine();
                }
            }
        }
        void Show_Ek()
        {
            Console.WriteLine("-----------------------------------------------------------------Ek");
            foreach (var E in Ek)
            {
                Console.WriteLine("--------Next layer");
                foreach (var value in E)
                {
                    Console.WriteLine("\t" + value);
                }
            }
        }
        void Show_BSk_and_BEk()
        {
            Console.WriteLine("-----------------------------------------------------------------BSk");
            foreach (var layer in BSk)
            {
                string result = string.Join("\t", layer);
                Console.WriteLine(result);
            }
            Console.WriteLine("-----------------------------------------------------------------BEk");
            string result1 = string.Join("\n", BEk);
            Console.WriteLine(result1);
        }
        void Show_LEk()
        {
            Console.WriteLine("-----------------------------------------------------------------LEk");
            foreach (var layer in LEk)
            {
                string result = string.Join("\t", layer);
                Console.WriteLine(result);
            }
        }
        void Show_fi_alfa_beta_gamma()
        {
            Console.WriteLine("-----------------------------------------------------------------fi");
            Console.WriteLine("\t" + fi);
            Console.WriteLine("-----------------------------------------------------------------alfa");
            Console.WriteLine("\t" + alfa);
            Console.WriteLine("-----------------------------------------------------------------beta");
            Console.WriteLine("\t" + beta);
            Console.WriteLine("-----------------------------------------------------------------gamma");
            Console.WriteLine("\t" + gamma);
        }
        void Show_r()
        {
            Console.WriteLine("-----------------------------------------------------------------r");
            foreach (var value in r)
            {
                Console.WriteLine(value);
            }
        }
        void Show_Xk1()
        {
            Console.WriteLine("-----------------------------------------------------------------Xk1");
            foreach (var X1 in Xk1)
            {
                Console.WriteLine("--------Next layer");
                foreach (var row in X1)
                {
                    foreach (var value in row)
                    {
                        Console.Write("\t" + value);
                    }
                    Console.WriteLine();
                }
            }
        }
        void Show_Best_Cost()
        {
            Console.WriteLine("-----------------------------------------------------------------BestCost");
            Console.WriteLine("\t" + E[0]);
        }
    }
}