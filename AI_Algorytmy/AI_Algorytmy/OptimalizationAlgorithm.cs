﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AI_Alogrytmy
{

    public class ParamInfo
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public double UpperBoundary { get; set; }
        public double LowerBoundary { get; set; }
    }

    public interface IOptimizationAlgorithm
    {
        string Name { get; set; }
        void Solve(FitnessFunction f, double[,] domain, params double[] parameters);
        ParamInfo[] ParamsInfo { get; set; }
        IStateWriter Writer { get; set; }
        IStateReader Reader { get; set; }
        IGenerateTextReport StringReportGenerator { get; set; }
        IGeneratePDFReport PdfReportGenerator { get; set; }
        double[] XBest { get; set; }
        double FBest { get; set; }
        int NumberOfEvaluationFitnessFunction { get; set; }
    }


    public class EvolutionaryAlgorithm : IOptimizationAlgorithm
    {
        private Random random = new Random();

        public string Name { get; set; }
        public double[] XBest { get; set; }
        public double FBest { get; set; }
        public int NumberOfEvaluationFitnessFunction { get; set; }
        public ParamInfo[] ParamsInfo { get; set; }
        public IStateWriter Writer { get; set; }
        public IStateReader Reader { get; set; }
        public IGenerateTextReport StringReportGenerator { get; set; }
        public IGeneratePDFReport PdfReportGenerator { get; set; }

        public void Solve(FitnessFunction f, double[,] domain, params double[] parameters)
        {
            // Parametry algorytmu ewolucyjnego
            int populationSize = (int)parameters[0];
            int maxGenerations = (int)parameters[1];
            double mutationRate = parameters[2];

            // Inicjalizacja populacji losowymi rozwiązaniami
            List<double[]> population = InitializePopulation(populationSize, domain);

            for (int generation = 0; generation < maxGenerations; generation++)
            {
                // Obliczanie wartości funkcji celu dla każdego osobnika
                List<double> fitnessValues = population.Select(individual => f(individual)).ToList();

                // Znalezienie najlepszego osobnika w populacji
                int indexOfBest = fitnessValues.IndexOf(fitnessValues.Min());
                XBest = population[indexOfBest];
                FBest = fitnessValues[indexOfBest];
                NumberOfEvaluationFitnessFunction = (generation + 1) * populationSize;

                // Zapis stanu algorytmu
                Writer.SaveToFileStateOfAlgorithm($"generation_{generation + 1}_state.txt");

                // Wygenerowanie raportu tekstowego
                string report = GenerateTextReport();
                Console.WriteLine(report);

                // Mutacja i krzyżowanie dla nowej populacji
                population = EvolvePopulation(population, fitnessValues, mutationRate, domain);
            }
        }

        private List<double[]> InitializePopulation(int size, double[,] domain)
        {
            List<double[]> population = new List<double[]>();

            for (int i = 0; i < size; i++)
            {
                double[] individual = new double[domain.GetLength(0)];
                for (int j = 0; j < individual.Length; j++)
                {
                    individual[j] = random.NextDouble() * (domain[j, 1] - domain[j, 0]) + domain[j, 0];
                }
                population.Add(individual);
            }

            return population;
        }

        private List<double[]> EvolvePopulation(List<double[]> population, List<double> fitnessValues, double mutationRate, double[,] domain)
        {
            List<double[]> newPopulation = new List<double[]>();

            for (int i = 0; i < population.Count; i += 2)
            {
                // Selekcja
                int parent1Index = SelectParentIndex(fitnessValues);
                int parent2Index = SelectParentIndex(fitnessValues);

                // Krzyżowanie
                double[] child1, child2;
                Crossover(population[parent1Index], population[parent2Index], out child1, out child2);

                // Mutacja
                Mutate(child1, mutationRate, domain);
                Mutate(child2, mutationRate, domain);

                newPopulation.Add(child1);
                newPopulation.Add(child2);
            }

            return newPopulation;
        }

        private int SelectParentIndex(List<double> fitnessValues)
        {
            // Prosta metoda ruletki
            double totalFitness = fitnessValues.Sum();
            double randomValue = random.NextDouble() * totalFitness;

            double currentSum = 0;
            for (int i = 0; i < fitnessValues.Count; i++)
            {
                currentSum += fitnessValues[i];
                if (currentSum >= randomValue)
                    return i;
            }

            // Domyślnie zwróć ostatni indeks
            return fitnessValues.Count - 1;
        }

        private void Crossover(double[] parent1, double[] parent2, out double[] child1, out double[] child2)
        {
            // Jednopunktowe krzyżowanie
            int crossoverPoint = random.Next(1, parent1.Length);

            child1 = new double[parent1.Length];
            child2 = new double[parent1.Length];

            Array.Copy(parent1, child1, crossoverPoint);
            Array.Copy(parent2, 0, child1, crossoverPoint, parent2.Length - crossoverPoint);

            Array.Copy(parent2, child2, crossoverPoint);
            Array.Copy(parent1, 0, child2, crossoverPoint, parent1.Length - crossoverPoint);
        }

        private void Mutate(double[] individual, double mutationRate, double[,] domain)
        {
            for (int i = 0; i < individual.Length; i++)
            {
                if (random.NextDouble() < mutationRate)
                {
                    individual[i] += (random.NextDouble() * 2 - 1) * (domain[i, 1] - domain[i, 0]) * 0.1;
                    individual[i] = Math.Max(domain[i, 0], Math.Min(domain[i, 1], individual[i]));
                }
            }
        }

        private string GenerateTextReport()
        {
            // Prosta implementacja raportu tekstowego
            return $"Best Solution (XBest): {string.Join(", ", XBest)}\nBest Fitness (FBest): {FBest}\nNumber of Evaluations: {NumberOfEvaluationFitnessFunction}";
        }
    }

    public class ExampleBenchmark
    {
        private readonly FitnessFunction fitnessFunction;

        public ExampleBenchmark(FitnessFunction fitnessFunction)
        {
            this.fitnessFunction = fitnessFunction;
        }

        public double Invoke(params double[] arg)
        {
            return fitnessFunction(arg);
        }
    }

}