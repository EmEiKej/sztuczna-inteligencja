﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using AI_Alogrytmy;

namespace AI_Alogrytmy
{
    public delegate double FitnessFunction(params double[] arg);

    public interface IStateWriter
    {
        void SaveToFileStateOfAlgorithm(string path);
    }

    public interface IStateReader
    {
        void LoadFromFileStateOfAlgorithm(string path);
    }

    public interface IGeneratePDFReport
    {
        void GenerateReport(string path);
    }

    public interface IGenerateTextReport
    {
        string ReportString { get; }
    }


    public class StateWriter : IStateWriter
    {
        public void SaveToFileStateOfAlgorithm(string path)
        {
            File.WriteAllText(path, "Stan algorytmu: ..."); // Zapisz odpowiednie dane
            Console.WriteLine("Zapisywanie stanu algorytmu do pliku: " + path);
        }
    }

    public class StateReader : IStateReader
    {
        public void LoadFromFileStateOfAlgorithm(string path)
        {
            Console.WriteLine("Wczytywanie stanu algorytmu z pliku: " + path);
        }
    }

    public class PdfReportGenerator : IGeneratePDFReport
    {
        public void GenerateReport(string path)
        {
            File.WriteAllText(path, "Generowanie raportu PDF: ..."); // Zapisz odpowiednie dane
            Console.WriteLine("Generowanie raportu PDF pod ścieżką: " + path);
        }
    }

    public class TextReportGenerator : IGenerateTextReport
    {
        public string ReportString { get; private set; }

        public TextReportGenerator(string report)
        {
            ReportString = report;
        }

        public void SaveToFile(string path)
        {
            File.WriteAllText(path, ReportString);
            Console.WriteLine("Zapisywanie raportu tekstowego do pliku: " + path);
        }
    }

    public class Program
    {
        static void Main()
        {
            // Przykładowe użycie algorytmu ewolucyjnego
            IOptimizationAlgorithm optimizationAlgorithm = new EvolutionaryAlgorithm();
            optimizationAlgorithm.Writer = new StateWriter(); // Prosta implementacja IStateWriter
            optimizationAlgorithm.PdfReportGenerator = new PdfReportGenerator(); // Prosta implementacja IGeneratePDFReport

            // Przykładowa funkcja celu (funkcja sferyczna)
            FitnessFunction benchmarkFunction = args => args.Sum(x => x * x);

            // Parametry algorytmu
            double[,] domain = { { -5.0, 5.0 }, { -5.0, 5.0 } };
            double[] algorithmParameters = { 50, 100, 0.1 };

            // Rozwiązanie optymalizacji
            optimizationAlgorithm.Solve(benchmarkFunction, domain, algorithmParameters);

            // Generowanie raportu do pliku PDF
            optimizationAlgorithm.PdfReportGenerator.GenerateReport("EvolutionaryAlgorithmReport.pdf");

            Console.WriteLine("Naciśnij dowolny klawisz, aby zakończyć...");
            Console.ReadKey();
        }
    }
}