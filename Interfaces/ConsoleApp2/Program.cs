﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Linq;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using Org.BouncyCastle;


/*
    Ważne info:

    Wymaga instalacji pakietów NuGet:
    itext7
    BouncyCastle.NetCore
    itext.bouncy-castle-fips adapter
    itext7.bouncy-castle-adapter
    itext7.pdfhtml

*/

namespace ConsoleApp2
{
    public delegate double FitnessFunction(params double[] arg);

    //Zapis
    public interface IStateWriter
    {
        void SaveToFileStateOfAlgorithm(string path);
    }

    public class StateWriter : IStateWriter
    {

        private int iterationNumber;
        private int functionCallsCount;
        private List<double[]> population;

        public StateWriter(int iterationNumber, int functionCallsCount, List<double[]> population)
        {
            this.iterationNumber = iterationNumber;
            this.functionCallsCount = functionCallsCount;
            this.population = population;
        }

        public void SaveToFileStateOfAlgorithm(string path)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.WriteLine($"Numer iteracji: {GetIterationNumber()}");
                    writer.WriteLine($"Liczba wywołań funkcji celu: {GetFunctionCallsCount()}");
                    for (int i = 0; i < population.Count; i++)
                    {
                        double[] individual = population[i];
                        string individualString = $"{i + 1}: {string.Join(" ", individual)}";
                        writer.WriteLine($"{individualString}");
                    }
                }

                Console.WriteLine("Zapisano stan algorytmu do pliku.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Wystąpił błąd podczas zapisywania stanu algorytmu: {ex.Message}");
            }
        }

        private int GetIterationNumber()
        {
            return 1;
        }

        private int GetFunctionCallsCount()
        {
            return 100;
        }

        private string GetPopulationInfo()
        {
            return "Meduzy";
        }
    }
    //Koniec zapisu


    //Czytanie
    public interface IStateReader
    {
        void LoadFromFileStateOfAlgorithm(string path);
    }
    public class StateReader : IStateReader
    {
        public void LoadFromFileStateOfAlgorithm(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    string iterationLine = reader.ReadLine();
                    string functionCallsLine = reader.ReadLine();
                    string populationLine = reader.ReadLine();

                    int iterationNumber = (int)ExtractValue(iterationLine);
                    int functionCallsCount = (int)ExtractValue(functionCallsLine);
                    string populationInfo = (string)ExtractValue(populationLine);

                    Console.WriteLine($"Loaded iteration number: {iterationNumber}");
                    Console.WriteLine($"Loaded function calls count: {functionCallsCount}");
                    Console.WriteLine($"Loaded population info: {populationInfo}");
                }

                Console.WriteLine("Wczytano stan algorytmu z pliku.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Wystąpił błąd podczas wczytywania stanu algorytmu: {ex.Message}");
            }
        }
        private object ExtractValue(string line)
        {
            string[] parts = line.Split(':');
            if (parts.Length == 2)
            {
                string trimmedValue = parts[1].Trim();
                if (int.TryParse(trimmedValue, out int intValue))
                {
                    return intValue;
                }
                else
                {
                    return trimmedValue;
                }
            }
            return null;
        }
    }
    //Koniec czytania

    //PDF RAPORT
    //Wymaga instalacji z pakietu NuGet itext7
    public interface IGeneratePDFReport
    {
        void GenerateReport(string path, string reportContent);
    }


    public class PdfReportGenerator : IGeneratePDFReport
    {
        public void GenerateReport(string path, string reportContent)
        {
            try
            {
                using (PdfWriter writer = new PdfWriter(path))
                {
                    using (PdfDocument pdf = new PdfDocument(writer))
                    {
                        Document document = new Document(pdf);
                        document.Add(new Paragraph(reportContent));
                        Console.WriteLine("PDF report generated successfully.");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error generating PDF report: {ex.Message}");

                Exception innerException = ex.InnerException;
                while (innerException != null)
                {
                    Console.WriteLine($"Inner Exception: {innerException.Message}");
                    innerException = innerException.InnerException;
                }
            }
        }
    }
    //Koniec PDF

    //Start Textraport
    public interface IGenerateTextReport
    {
        string ReportString { get; }
    }
    public class TextReportGenerator : IGenerateTextReport
    {
        private readonly int iterationNumber;
        private readonly int functionCallsCount;
        private readonly List<double[]> population;


        //Dodanie z maina do klasy zmiennych
        public TextReportGenerator(int iterationNumber, int functionCallsCount, List<double[]> population)
        {
            this.iterationNumber = iterationNumber;
            this.functionCallsCount = functionCallsCount;
            this.population = population;
        }

        public string ReportString
        {
            get
            {
                string bestIndividualInfo = "Najlepszy osobnik: ...";
                string functionCallsInfo = $"Liczba wywołań funkcji celu: {functionCallsCount}";
                string algorithmParametersInfo = $"Numer iteracji: {iterationNumber}\nParametry algorytmu: ...";

                //Populacja
                StringBuilder populationInfo = new StringBuilder("Populacja:\n");
                for (int i = 0; i < population.Count; i++)
                {
                    double[] individual = population[i];
                    string individualString = $"{i + 1}: {string.Join(" ", individual)}";
                    populationInfo.AppendLine($"{individualString}");
                }

                return $"{bestIndividualInfo}\n{functionCallsInfo}\n{algorithmParametersInfo}\n{populationInfo}";
            }
        }
    }


    //Koniec Text report

    public class Program
    {
        static void Main()
        {
            string filePath = "./../../";

            //Writer
            int iterationNumber = 5;
            int functionCallsCount = 87;
            List<double[]> population = new List<double[]>
            {
                new double[] { 1.0, 2.0, 3.0 },
                new double[] { 4.0, 5.0, 6.0 },
            };
            StateWriter stateWriter = new StateWriter(iterationNumber, functionCallsCount, population);
            stateWriter.SaveToFileStateOfAlgorithm(filePath + "zapis.txt");

            //Reader
            IStateReader reader = new StateReader();
            reader.LoadFromFileStateOfAlgorithm(filePath + "zapis.txt");

            //PDF
            IGeneratePDFReport pdfReportGenerator = new PdfReportGenerator();
            string reportContent = "TU PRZEKAŻ STRINGA MORDO"; //!!!! ! !     !! ! ! !  PDF
            pdfReportGenerator.GenerateReport(filePath + "raport.pdf", reportContent);

            //Text raport
            IGenerateTextReport textReportGenerator = new TextReportGenerator(iterationNumber, functionCallsCount, population);
            string report = textReportGenerator.ReportString;
            Console.WriteLine("Wygenerowany raport:");
            Console.WriteLine(report);
            Console.WriteLine("Naciśnij dowolny klawisz, aby zakończyć...");
            Console.ReadKey();
        }
    }
}
