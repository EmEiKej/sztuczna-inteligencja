﻿//Nazwa pliku musibyć taka sama jak namespace,nazwa klasy zawsze taka sama,Optymalization algoritm, pochodna interfacu IOptymalizationAlgorithm i wartosci wszedzie tak samo
using System.Reflection;
using System.Xml.Linq;
using Atomic_orbital_search;
List<string> algoritms = new List<string>();
object[] constructorValues = { E_Test_Functions_Names.HimmelblausFunctionN6, -1, 1, 20, 30, 100, 0.1, 0.01 };
object[] functionValues = null;

ReferenceMenager referenceMenager = new ReferenceMenager();
referenceMenager.UpdateReferences();
algoritms = referenceMenager.GetReferences();
referenceMenager.RunReferenceFunction(algoritms[2], "AOS_Main_Function", constructorValues, functionValues);
public class ReferenceMenager
{
    List<string> references = new List<string>();
    string Klasy_Folder_Path;
    string csproj_Path;
    public void UpdateReferences()
    {
        string Project_Path = Directory.GetCurrentDirectory();
        //Tu by sie jeszcze przydało zabespieczenie na wypadek jak Project_Path.Length - 16 bedzie mniejsze od 0.
        if (CheckIfPathIsPossible(Project_Path))
        {
            Klasy_Folder_Path = Project_Path.Substring(0, Project_Path.Length - 16) + "Klasy";
            string[] csprojFiles = Directory.GetFiles(Project_Path.Substring(0, Project_Path.Length - 16), "*.csproj");
            csproj_Path = csprojFiles.FirstOrDefault();
        }
        if (CheckIf_csproj_FilePathIsCorrect(csproj_Path))
        {
            ClearAllReferences(csproj_Path);
            if (CheckIfDiractoryExists(Klasy_Folder_Path))
            {
                string[] dll_Files = Directory.GetFiles(Klasy_Folder_Path, "*.dll");
                foreach (string dll_File in dll_Files)
                {
                    AddReferenceToProject(csproj_Path, dll_File);
                }
                SaveAllReferences(csproj_Path, references);
            }

        }
    }
    bool CheckIfPathIsPossible(string projectPath)
    {
        if (projectPath.Length - 16 > 0)
        {
            return true;
        }
        else
        {
            Console.WriteLine("Unexpected error.");
            return false;
        }
    }
    bool CheckIf_csproj_FilePathIsCorrect(string csprojFile)
    {
        if (csprojFile != string.Empty)
        {
            return true;
        }
        else
        {
            Console.WriteLine(".csproj file does not exist.");
            return false;
        }
    }
    void ClearAllReferences(string projectFilePath)
    {
        XDocument projectFile = XDocument.Load(projectFilePath);
        XElement itemGroup = projectFile.Root.Elements("ItemGroup").FirstOrDefault(ig => ig.Elements("Reference").Any());
        if (itemGroup == null)
        {
            return;
        }
        itemGroup.RemoveAll();
        projectFile.Save(projectFilePath);
    }
    bool CheckIfDiractoryExists(string directory)
    {
        if (Directory.Exists(directory))
        {
            return true;
        }
        else
        {
            Console.WriteLine($"Folder {directory} does not exist.");
            return false;
        }
    }
    bool CheckIfFileExists(string directory)
    {
        if (File.Exists(directory))
        {
            return true;
        }
        else
        {
            Console.WriteLine($"File {directory} does not exist.");
            return false;
        }
    }
    void AddReferenceToProject(string projectFilePath, string dllPath)
    {
        XDocument projectFile = XDocument.Load(projectFilePath);
        XElement itemGroup = projectFile.Root.Elements("ItemGroup").FirstOrDefault(ig => ig.Elements("Reference").Any());
        if (itemGroup == null)
        {
            itemGroup = new XElement("ItemGroup");
            projectFile.Root.Add(itemGroup);
        }
        itemGroup.Add(new XElement("Reference", new XAttribute("Include", Path.GetFileNameWithoutExtension(dllPath)), new XElement("HintPath", dllPath)));
        projectFile.Save(projectFilePath);
    }
    void SaveAllReferences(string projectFilePath, List<string> _references)
    {
        XDocument projectFile = XDocument.Load(projectFilePath);
        var references = projectFile.Descendants("ItemGroup").Elements("Reference").Select(reference => new { Include = reference.Attribute("Include")?.Value });
        foreach (var reference in references)
        {
            _references.Add(reference.Include);
        }
    }
    public void RunReferenceFunction(string chosenAlgorithm, string chosenFunction, object[] constructorValues, object[] functionValues)
    {
        if (CheckIfFileExists(Klasy_Folder_Path + "\\" + chosenAlgorithm + ".dll"))
        {
            Assembly externalAssembly = Assembly.LoadFrom(Klasy_Folder_Path + "\\" + chosenAlgorithm + ".dll");
            chosenAlgorithm = ReplaceSpace(chosenAlgorithm);
            string chosenNamespaceAndClass;
            chosenNamespaceAndClass = FindClass(externalAssembly, chosenAlgorithm);
            if (chosenNamespaceAndClass != null) 
            {
                Type yourClassType = externalAssembly.GetType(chosenNamespaceAndClass);
                var instance = Activator.CreateInstance(yourClassType, constructorValues);
                if (MethodExists(yourClassType, chosenFunction))
                {
                    //Trzeba uwarunkowac w zalerznosci od wybtranej metody
                    var result = /*(ValueTuple<double, List<double>>)*/yourClassType.GetMethod(chosenFunction).Invoke(instance, functionValues);
                    Console.WriteLine(result);
                }
            }
        }
    }
    string FindClass(Assembly assembly, string chosenNamespace)
    {
        Type[] types = assembly.GetTypes();
        foreach (Type type in types)
        {
            if (type.IsClass)
            {
                if (type.FullName.Contains(chosenNamespace))
                {
                    return type.FullName;
                }
            }
        }
        Console.WriteLine($"There is no class in {chosenNamespace} namespace");
        return null;
    }
    bool MethodExists(Type type, string chosenMethod)
    {
        foreach (MethodInfo method in type.GetMethods())
        {
            if (chosenMethod == method.Name) { return true; }
        }
        Console.WriteLine($"{chosenMethod} function does not exist.");
        return false;
    }
    string ReplaceSpace(string algorithm_Name)
    {
        for (int i = 0; i < algorithm_Name.Length; i++)
        {
            if (algorithm_Name[i] == ' ')
            {
                char[] charArray = algorithm_Name.ToCharArray();
                charArray[i] = '_';
                return new string(charArray);
            }
        }
        return algorithm_Name;
    }
    public List<string> GetReferences()
    {
        return references;
    }
}



